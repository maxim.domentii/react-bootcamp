import { Component } from 'react';
import Bulb from 'react-bulb';

import './light-bulb.css';

export default class LightBulb extends Component {

    state = {
        on: false
    }

    turnLightOnOff = () => {
        this.setState((state) =>{
            return {
                on: !state.on
            }
        });
    }

    render() {
        const color = this.state.on ? 'yellow' : 'white';

        return (
            <div className="light-bulb">
                <Bulb size={200} color={color}/>
                <button onClick={this.turnLightOnOff}
                        type="button"
                        className="btn btn-outline-dark">Turn light on/off</button>
            </div>
        );
    };
}