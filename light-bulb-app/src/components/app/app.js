import LightBulb from "../light-bulb";

import './app.css'

const App = () => {
    return (
        <div className="app">
            <LightBulb/>
        </div>
    );
};

export default App;