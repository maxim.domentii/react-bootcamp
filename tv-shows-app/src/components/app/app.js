import 'semantic-ui-css/semantic.min.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {Container, Divider, Header} from "semantic-ui-react";
import Popular from "../../screens/popular";
import Details from "../../screens/details"


const App = () => {
    return (
        <Container>
            <Header as='h1'>TV Shows</Header>
            <Divider/>
            <Router>
                <Switch>
                    <Route exact path='/'>
                        <Popular/>
                    </Route>
                    <Route path="/:id" children={<Details />} />
                </Switch>
            </Router>
        </Container>
    )
};

export default App;