import {useState} from 'react';
import ShowList from "./components/show-list";
import {xor} from 'lodash';
import {Container, Input} from "semantic-ui-react";
import usePopularShowsApi from "./hooks/usePopularShowsApi";

const Popular = () => {
    const [searchText, setSearchText] = useState('');
    const [favorites, setFavorites] = useState([]);
    const {popularShows, isShowsLoaded, error} = usePopularShowsApi();

    const handleSearchFiledSearch = (event) => {
        setSearchText(event.target.value)
    };

    const filterShow = () => {
        if (!isShowsLoaded) {
            return [];
        }

        return popularShows.filter(
            (show) => show.name.toLocaleLowerCase().includes(searchText.toLocaleLowerCase()));
    };

    const markAsFavorite = (id) => {
        setFavorites(xor(favorites, [id]));
    };

    return (
        <Container>
            <Input
                placeholder="Search shows..."
                value={searchText}
                onChange={handleSearchFiledSearch}/>
            <ShowList
                shows={filterShow()}
                favorites={favorites}
                markAsFavorite={markAsFavorite}
                isShowsLoaded={isShowsLoaded}
                error={error}/>
        </Container>
    );
};

export default Popular;