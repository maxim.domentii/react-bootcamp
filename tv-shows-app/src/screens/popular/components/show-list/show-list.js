import ShowItem from "../show-item";
import {Card, Dimmer, Loader, Message, Segment} from "semantic-ui-react";

const ShowList = ({shows, markAsFavorite, favorites, isShowsLoaded, error}) => {
    if (!isShowsLoaded) {
        return (
            <Dimmer active inverted>
                <Loader>Loading</Loader>
            </Dimmer>
        )
    }

    if (error) {
        return (
            <Message negative>
                <Message.Header>We are sorry. Try it later!</Message.Header>
            </Message>
        )
    }

    return (
        <Segment>
            <Card.Group doubling itemsPerRow={4}>
                {
                    shows.map(({id, poster_path, name, vote_average, first_air_date}) => (
                        <ShowItem
                            key={id}
                            id={id}
                            name={name}
                            posterPath={poster_path}
                            rating={vote_average}
                            releaseDate={first_air_date}
                            favorite={favorites.some((favorite) => id === favorite)}
                            markAsFavorite={() => markAsFavorite(id)}
                        />
                    ))
                }
            </Card.Group>
        </Segment>
    );
};

export default ShowList;