import {Button, Card, Icon, Image} from "semantic-ui-react";
import {Link} from "react-router-dom";

const ShowItem = ({id, name, rating, posterPath, favorite, releaseDate, markAsFavorite}) => {
    const style = {
        cursor: 'pointer'
    };

    const handleMarkAsFavorite = (event) => {
        event.preventDefault();
        markAsFavorite();
    };

    return (
        <Card style={style} as={Link} to={`/${id}`}>
            <Image
                src={`https://image.tmdb.org/t/p/w500/${posterPath}`}
                alt={name}
            />
            <Card.Content>
                <Card.Header>{name}</Card.Header>
                <Card.Description>{releaseDate}</Card.Description>
            </Card.Content>
            <Card.Content>
                <Icon name='star'/>
                {rating}
            </Card.Content>
            <Card.Content>
                <Button fluid basic={!favorite} positive={favorite} onClick={handleMarkAsFavorite}>
                    <Icon name='heart'/>
                    {favorite ? 'Remove from vavorites' : 'Add to favorites'}
                </Button>
            </Card.Content>
        </Card>
    );
};

export default ShowItem;