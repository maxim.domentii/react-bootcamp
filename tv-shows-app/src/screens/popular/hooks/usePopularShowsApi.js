import {useEffect, useState} from "react";

const usePopularShowsApi = () => {
    const [popularShows, setPopularShows] = useState([]);
    const [isShowsLoaded, setIsShowsLoaded] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        const params = new URLSearchParams({
            api_key: process.env.REACT_APP_API_KEY,
            language: 'en_US',
            page: 1
        })
        fetch(`https://api.themoviedb.org/3/tv/popular?${params}`)
            .then((res) => res.json())
            .then(
                (response) => {
                    if (response.results) {
                        setPopularShows(response.results);
                        setIsShowsLoaded(true);
                        setError(null);
                    } else {
                        setPopularShows([]);
                        setIsShowsLoaded(true);
                        setError(response.status_message);
                    }
                },
                (error) => {
                    console.log(error);
                    setPopularShows([]);
                    setIsShowsLoaded(true);
                    setError(error);
                });

        // cleanup function
        return () => {};
    }, []);

    return {
        popularShows,
        isShowsLoaded,
        error
    }
};

export default usePopularShowsApi;