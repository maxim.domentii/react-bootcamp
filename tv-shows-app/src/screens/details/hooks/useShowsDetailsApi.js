import {useEffect, useState} from "react";

const useShowsDetailsApi = (id) => {
    const [details, setDetails] = useState([]);
    const [isShowLoaded, setIsShowLoaded] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        const params = new URLSearchParams({
            api_key: process.env.REACT_APP_API_KEY,
            language: 'en_US'
        })
        fetch(`https://api.themoviedb.org/3/tv/${id}?${params}`)
            .then((res) => res.json())
            .then(
                (response) => {
                    if (response.id) {
                        setDetails(response);
                        setIsShowLoaded(true);
                        setError(null);
                    } else {
                        setDetails(null);
                        setIsShowLoaded(true);
                        setError(response.status_message);
                    }
                },
                (error) => {
                    console.log(error);
                    setIsShowLoaded(null);
                    setIsShowLoaded(true);
                    setError(error);
                });

        // cleanup function
        return () => {};
    }, [id]);

    return {
        details,
        isShowLoaded,
        error
    }
};

export default useShowsDetailsApi;