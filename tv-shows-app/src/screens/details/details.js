import useShowsDetailsApi from "./hooks/useShowsDetailsApi";
import {
    Container,
    Dimmer,
    Divider,
    Item,
    Loader,
    Message,
    Segment,
    Breadcrumb,
    Header,
    Label
} from "semantic-ui-react";
import {useParams, Link} from "react-router-dom";
import "./details.css"

const Details = () => {
    const {id} = useParams();
    const {details, isShowLoaded, error} = useShowsDetailsApi(id);
    const {poster_path, backdrop_path, name, overview, genres} = details;

    if (!isShowLoaded) {
        return (
            <Dimmer active inverted>
                <Loader>Loading</Loader>
            </Dimmer>
        )
    }

    if (error) {
        return (
            <Message negative>
                <Message.Header>We are sorry. Try it later!</Message.Header>
            </Message>
        )
    }

    return (
        <Container>
            <Breadcrumb>
                <Breadcrumb.Section link>
                    <Link to="/">Home</Link>
                </Breadcrumb.Section>
                <Breadcrumb.Divider/>
                <Breadcrumb.Section link>
                    {name}
                </Breadcrumb.Section>
            </Breadcrumb>
            <Segment
                inverted
                style={{
                    backgroundImage: `url(https://image.tmdb.org/t/p/w500/${backdrop_path})`,
                    backgroundSize: 'cover',
                }}>
                <Item.Group>
                    <Item>
                        <Item.Image size='medium' src={`https://image.tmdb.org/t/p/w500/${poster_path}`}/>
                        <Item.Content>
                            <Header as='h1' size='huge' inverted style={{
                                color: '#ffffff',
                            }}>{name}</Header>
                            <Item.Meta>
                                {genres.map(({id, name}) => (
                                    <Label key={id}>{name}</Label>
                                ))}
                            </Item.Meta>
                            <Divider/>
                            <Item.Description style={{
                                color: '#ffffff',
                                fontWeight: 'bold'
                            }}>{overview}</Item.Description>
                        </Item.Content>
                    </Item>
                </Item.Group>
            </Segment>
        </Container>
    );
};

export default Details;