import { Component } from 'react';
import './todo-list-item.css';

export default class TodoListItem extends Component {

  render() {
    const { important, label, done, onDelete, onLabelClick, onImportant } = this.props;

    const style = {
      color: important ? 'steelblue' : 'black',
      fontWeight: important ? 'bold' : 'normal',
      textDecoration: done ? 'line-through': null
    };

    return (
      <span className="todo-list-item">
      <span
        className="todo-list-item-label"
        style={style}
        onClick={onLabelClick}>
        {label}
      </span>

      <button type="button"
              className="btn btn-outline-success btn-sm float-right"
              onClick={onImportant}>
        <i className="fa fa-exclamation" />
      </button>

      <button type="button"
              className="btn btn-outline-danger btn-sm float-right"
              onClick={onDelete}>
        <i className="fa fa-trash-o" />
      </button>
    </span>
    );
  }
}
