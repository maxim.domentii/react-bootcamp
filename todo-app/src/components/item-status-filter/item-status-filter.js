import './item-status-filter.css';

const ItemStatusFilter = ({onFilter}) => {
  return (
    <div className="btn-group">
      <button type="button"
              className="btn btn-info"
              onClick={() => onFilter(
                  {
                      all: true,
                      active: false,
                      done: false
                  })}>All</button>
      <button type="button"
              className="btn btn-outline-secondary"
              onClick={() => onFilter(
                  {
                      all: false,
                      active: true,
                      done: false
                  })}>Active</button>
      <button type="button"
              className="btn btn-outline-secondary"
              onClick={() => onFilter(
                  {
                      all: false,
                      active: false,
                      done: true
                  })}>Done</button>
    </div>
  );
};

export default ItemStatusFilter;
