import {Component} from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';
import NewItemInput from "../new-item-input";

import './app.css';

export default class App extends Component {

    state = {
        searchText: null,
        filters: {
            all: true,
            active: false,
            done: false
        },
        todoData: [
            {label: 'Drink Coffee', important: false, done: false, id: 1},
            {label: 'Make Awesome App', important: true, done: false, id: 2},
            {label: 'Have a lunch', important: false, done: false, id: 3}
        ]
    };

    onDelete = (id) => {
        this.setState(({todoData}) => {
            const newTodoData = todoData.filter((el) => el.id !== id);

            return {
                todoData: newTodoData
            }
        });
    };

    onAdd = (label) => {
        label !== null && label !== "" && this.setState(({todoData}) => {
            const maxId = todoData.map(({id}) => id).reduce((curr, prev) => curr > prev ? curr : prev);

            const newTodoData = [
                ...todoData,
                {
                    label: label,
                    important: false,
                    done: false,
                    id: maxId + 1
                }
            ];

            return {
                todoData: newTodoData
            }
        });
    };

    onLabelClick = (id) => {
        this.toggleProp(id, 'done');
    };

    onImportant = (id) => {
        this.toggleProp(id, 'important');
    }

    toggleProp(id, prop) {
        this.setState(({todoData}) => {
            const newTodoData = todoData.map((el) => el.id !== id ? el : { ...el, [prop]: !el[prop], });

            return {
                todoData: newTodoData
            }
        });
    };

    onFilter = (filters) => {
        this.setState({
            filters: filters
        });
    };

    onChange = (text) => {
        this.setState({
            searchText: text.length === 0 ? null : text
        });
    };

    render() {
        const { todoData, filters, searchText } = this.state;
        let filteredData = todoData.filter((el) => {
            if (filters.all === true){
                return true;
            } else if (filters.active === true){
                return !el.done;
            } else if (filters.done === true) {
                return el.done;
            }
            return true;
        })
        filteredData = searchText === null ?
            filteredData
            : filteredData.filter(({ label }) => label.toLowerCase().includes(searchText.toLocaleLowerCase()));

        const { toDo, done } = {
            toDo: todoData.filter(({ done }) => !done).length,
            done: todoData.filter(({ done }) => done).length
        }

        return (
            <div className="todo-app">
                <AppHeader toDo={toDo} done={done}/>
                <div className="top-panel d-flex">
                    <SearchPanel onChange={this.onChange}/>
                    <ItemStatusFilter onFilter={this.onFilter}/>
                </div>

                <TodoList todos={filteredData}
                          onDelete={this.onDelete}
                          onLabelClick={this.onLabelClick}
                          onImportant={this.onImportant}/>

                <div className="top-panel d-flex">
                    <NewItemInput onAdd={this.onAdd}/>
                </div>
            </div>
        );
    }
};
