import {Component} from 'react';

import './new-item-input.css';

export default class NewItemInput extends Component {

    state = {
        label: ""
    }

    setLabel(label) {
        this.setState({
            label: label
        });
    };

    onSubmit = (event) => {
        event.preventDefault();

        const {label} = this.state;
        if (label.length === 0) {
            return;
        }

        const {onAdd} = this.props;
        onAdd(label);

        this.setLabel("");
    }

    render() {
        return (
            <form className="btn-group" onSubmit={this.onSubmit}>
                <input type="text"
                       className="form-control new-item-input"
                       placeholder="type here new item"
                       value={this.state.label}
                       onChange={event => this.setLabel(event.target.value)}
                />

                <button className="btn btn-info">Add</button>
            </form>
        );
    }
}
