import './search-panel.css';

const SearchPanel = ({ onChange }) => {

  return (
    <input type="text"
           className="form-control search-input"
           placeholder="type to search"
           onChange={event => onChange(event.target.value)}
    />
  );
};

export default SearchPanel;
