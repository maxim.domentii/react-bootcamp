const test1Data = [2, 4, 6, 8];
const test1Expected = 20;

const test2Data = [1, 3, 5, 7];
const test2Expected = 1;

const test3Data = [2, 3, 4, 5, 6];
const test3Expected = 10;
