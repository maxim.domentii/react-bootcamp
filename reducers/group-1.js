const test1Data = ["A", "b", "C", "d", "E"];
const test1Expected = "ACE";

const test2Data = [""];
const test2Expected = "";

const test3Data = ["a", "b", "c", "d"];
const test3Expected = "";

const test4Data = ["a", "b", "c", "D"];
const test4Expected = "D";
