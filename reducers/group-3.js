const test1Data = [1, 2, 3, 4];
const test1Expected = { even: 6, odd: 4 };

const test2Data = [1, 3, 5, 7];
const test2Expected = { even: 0, odd: 16 };

const test3Data = [2, 4, 6];
const test3Expected = { odd: 0, even: 12 };
